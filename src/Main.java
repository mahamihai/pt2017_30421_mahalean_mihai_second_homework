import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.awt.Button;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class Main {
	
	public int min_i;
	public int max_i;
	public int min_service;
	public int max_service;
	public int nr_queues=0;
	public int interval;
	public QueueManager qm;
	public int current_time=0;
	public JButton start ;
	 int arrival_delay=0;
	 public JTextField mini;
	 public JTextField maxi;
	 public JTextField mins;
	 public JTextField maxs;
	 public JTextField ques_nr;
	 private JFrame jf;
	 public JTextField interval_in;
	 private JPanel panel;
	public void read_input() throws FileNotFoundException
	{
		Scanner scanner = new Scanner(new File("in.txt"));
		 min_i=scanner.nextInt();
		 
		 System.out.println("here"+min_i);
		 max_i=scanner.nextInt();
		 min_service=scanner.nextInt();
		 max_service=scanner.nextInt();
		 nr_queues=scanner.nextInt();
		 interval=scanner.nextInt();
		scanner.close();
		
		
	}
	public Main() throws FileNotFoundException
	{
		read_input();
		
		
		
	}
	private void add_frames()
	{
		for(int i=0;i<this.nr_queues;i++)
		{
			JTextArea aux=new JTextArea ();
			aux.setText("Queue "+i+":");
			aux.setMaximumSize(new Dimension(1000,150));
			displays.add(aux);
			JScrollPane result=new JScrollPane(aux);
			//result.setSize(new Dimension(1000,150));
			panel.add(aux);
		}
		
	}
	private ArrayList<JTextArea> displays;
	void setup_gui()
	{
		JLabel min_i_label=new JLabel("Minium Interval:");
		mini=new JTextField();
		mini.setMaximumSize(new Dimension(1000,40));
		JLabel max_i_label=new JLabel("Maximum Interval:");
		maxi=new JTextField();
		maxi.setMaximumSize(new Dimension(1000,40));
		JLabel min_s=new JLabel("Minium Service:");
		mins=new JTextField();
		mins.setMaximumSize(new Dimension(1000,40));
		JLabel max_s=new JLabel("Maximum Service:");
		maxs=new JTextField();
		maxs.setMaximumSize(new Dimension(1000,40));
		JLabel ques=new JLabel("Nr_of_queues");
		ques_nr=new JTextField();
		ques_nr.setMaximumSize(new Dimension(1000,40));
		JLabel interval_l=new JLabel("Number of seconds to simulate:");
		interval_in=new JTextField();
		interval_in.setMaximumSize(new Dimension(1000,40));
		


		jf = new JFrame("Queues");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setSize(700, 1000);
		jf.setLocation(300, 300);	//setup the frame dimensions
		panel = new JPanel();
		
		
		
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	
		displays=new ArrayList<JTextArea>();

		panel.add(min_i_label);
		panel.add(mini);
		panel.add(max_i_label);
		panel.add(maxi);
		
		panel.add(min_s);
		panel.add(mins);
		panel.add(max_s);
		panel.add(maxs);
		panel.add(ques);
		panel.add(ques_nr);
		panel.add(interval_l);
		panel.add(interval_in);
		start= new JButton("Start simulation"); // implement add button
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				min_i=Integer.parseInt(mini.getText());
				max_i=Integer.parseInt(maxi.getText());
				min_service=Integer.parseInt(mins.getText());
				max_service=Integer.parseInt(maxs.getText());
				nr_queues=Integer.parseInt(ques_nr.getText());
				interval=Integer.parseInt(interval_in.getText());
				try {
					run_simulation();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				
			}
		});
		panel.add(start);
		jf.add(panel);
		jf.setVisible(true);
	}
	public int create_person()
	{
		Random rand=new Random();
		int service_time=rand.nextInt(max_service-min_service+1)+min_service;
		
		return service_time;
		
		
	}
	int check_arrival()
	{
		Random rand=new Random();
		if(this.arrival_delay==0)
		{
			this.arrival_delay=new Random().nextInt(max_i-min_i+1)+min_i;
			return 1;
		}
		else
		{
			arrival_delay--;
			return 0;
		}
			
	}
	
	public void run_simulation() throws InterruptedException
	{
		qm=new QueueManager(nr_queues);
		this.add_frames();
		qm.set_views(displays);
		Thread thread=new Thread(new Runnable()
		{
			public void run()
			{
				for(int i=0;i<interval;i++)
				{
					
				qm.pass_second(current_time);
				current_time++;
				if(check_arrival()==1)
				{
					qm.add_person(create_person(),current_time);
				}	
				System.out.println("Current time is:"+current_time);
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				}
				
			}
		}
		);
		
			thread.start();		
		
		
	}
	
	public static void main(String[] args) throws FileNotFoundException, InterruptedException
	{
		Main main=new Main();
		main.setup_gui();
		//main.run_simulation();
		
		
		
		
		
		
	}
	
	
	

}
